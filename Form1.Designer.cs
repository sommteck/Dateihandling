namespace Dateihandling
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelˆscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode f¸r die Designerunterst¸tzung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor ge‰ndert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_end = new System.Windows.Forms.Button();
            this.cmd_file_read_all = new System.Windows.Forms.Button();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.cmd_quellpfad = new System.Windows.Forms.Button();
            this.cmd_zielpfad = new System.Windows.Forms.Button();
            this.cmd_file_read_byte = new System.Windows.Forms.Button();
            this.cmd_file_read_row = new System.Windows.Forms.Button();
            this.cmd_file_write = new System.Windows.Forms.Button();
            this.cmd_file_copy = new System.Windows.Forms.Button();
            this.cmd_file_delete = new System.Windows.Forms.Button();
            this.cmd_filename = new System.Windows.Forms.Button();
            this.listBox_status = new System.Windows.Forms.ListBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.txt_filename = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(635, 23);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(124, 25);
            this.cmd_end.TabIndex = 0;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // cmd_file_read_all
            // 
            this.cmd_file_read_all.Location = new System.Drawing.Point(635, 190);
            this.cmd_file_read_all.Name = "cmd_file_read_all";
            this.cmd_file_read_all.Size = new System.Drawing.Size(124, 29);
            this.cmd_file_read_all.TabIndex = 1;
            this.cmd_file_read_all.Text = "Datei lesen Alle";
            this.cmd_file_read_all.UseVisualStyleBackColor = true;
            this.cmd_file_read_all.Click += new System.EventHandler(this.cmd_file_read_all_Click);
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(13, 23);
            this.txt_output.Multiline = true;
            this.txt_output.Name = "txt_output";
            this.txt_output.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_output.Size = new System.Drawing.Size(527, 196);
            this.txt_output.TabIndex = 2;
            // 
            // cmd_quellpfad
            // 
            this.cmd_quellpfad.Location = new System.Drawing.Point(635, 54);
            this.cmd_quellpfad.Name = "cmd_quellpfad";
            this.cmd_quellpfad.Size = new System.Drawing.Size(124, 26);
            this.cmd_quellpfad.TabIndex = 3;
            this.cmd_quellpfad.Text = "Quellpfad";
            this.cmd_quellpfad.UseVisualStyleBackColor = true;
            this.cmd_quellpfad.Click += new System.EventHandler(this.cmd_quellpfad_Click);
            // 
            // cmd_zielpfad
            // 
            this.cmd_zielpfad.Location = new System.Drawing.Point(635, 87);
            this.cmd_zielpfad.Name = "cmd_zielpfad";
            this.cmd_zielpfad.Size = new System.Drawing.Size(124, 26);
            this.cmd_zielpfad.TabIndex = 4;
            this.cmd_zielpfad.Text = "Zielpfad";
            this.cmd_zielpfad.UseVisualStyleBackColor = true;
            this.cmd_zielpfad.Click += new System.EventHandler(this.cmd_zielpfad_Click);
            // 
            // cmd_file_read_byte
            // 
            this.cmd_file_read_byte.Location = new System.Drawing.Point(635, 119);
            this.cmd_file_read_byte.Name = "cmd_file_read_byte";
            this.cmd_file_read_byte.Size = new System.Drawing.Size(124, 28);
            this.cmd_file_read_byte.TabIndex = 5;
            this.cmd_file_read_byte.Text = "Datei lesen Byte";
            this.cmd_file_read_byte.UseVisualStyleBackColor = true;
            this.cmd_file_read_byte.Click += new System.EventHandler(this.cmd_file_read_byte_Click);
            // 
            // cmd_file_read_row
            // 
            this.cmd_file_read_row.Location = new System.Drawing.Point(635, 153);
            this.cmd_file_read_row.Name = "cmd_file_read_row";
            this.cmd_file_read_row.Size = new System.Drawing.Size(124, 29);
            this.cmd_file_read_row.TabIndex = 6;
            this.cmd_file_read_row.Text = "Datei lesen Zeile";
            this.cmd_file_read_row.UseVisualStyleBackColor = true;
            this.cmd_file_read_row.Click += new System.EventHandler(this.cmd_file_read_row_Click);
            // 
            // cmd_file_write
            // 
            this.cmd_file_write.Location = new System.Drawing.Point(635, 225);
            this.cmd_file_write.Name = "cmd_file_write";
            this.cmd_file_write.Size = new System.Drawing.Size(124, 27);
            this.cmd_file_write.TabIndex = 7;
            this.cmd_file_write.Text = "Datei schreiben";
            this.cmd_file_write.UseVisualStyleBackColor = true;
            this.cmd_file_write.Click += new System.EventHandler(this.cmd_file_write_Click);
            // 
            // cmd_file_copy
            // 
            this.cmd_file_copy.Location = new System.Drawing.Point(635, 259);
            this.cmd_file_copy.Name = "cmd_file_copy";
            this.cmd_file_copy.Size = new System.Drawing.Size(124, 25);
            this.cmd_file_copy.TabIndex = 8;
            this.cmd_file_copy.Text = "Datei kopieren";
            this.cmd_file_copy.UseVisualStyleBackColor = true;
            this.cmd_file_copy.Click += new System.EventHandler(this.cmd_file_copy_Click);
            // 
            // cmd_file_delete
            // 
            this.cmd_file_delete.Location = new System.Drawing.Point(635, 291);
            this.cmd_file_delete.Name = "cmd_file_delete";
            this.cmd_file_delete.Size = new System.Drawing.Size(124, 26);
            this.cmd_file_delete.TabIndex = 9;
            this.cmd_file_delete.Text = "Datei lˆschen";
            this.cmd_file_delete.UseVisualStyleBackColor = true;
            this.cmd_file_delete.Click += new System.EventHandler(this.cmd_file_delete_Click);
            // 
            // cmd_filename
            // 
            this.cmd_filename.Location = new System.Drawing.Point(635, 324);
            this.cmd_filename.Name = "cmd_filename";
            this.cmd_filename.Size = new System.Drawing.Size(124, 26);
            this.cmd_filename.TabIndex = 10;
            this.cmd_filename.Text = "Dateiname";
            this.cmd_filename.UseVisualStyleBackColor = true;
            this.cmd_filename.Click += new System.EventHandler(this.cmd_filename_Click);
            // 
            // listBox_status
            // 
            this.listBox_status.FormattingEnabled = true;
            this.listBox_status.ItemHeight = 16;
            this.listBox_status.Location = new System.Drawing.Point(12, 233);
            this.listBox_status.Name = "listBox_status";
            this.listBox_status.ScrollAlwaysVisible = true;
            this.listBox_status.Size = new System.Drawing.Size(528, 212);
            this.listBox_status.TabIndex = 11;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txt_filename
            // 
            this.txt_filename.Location = new System.Drawing.Point(626, 385);
            this.txt_filename.Name = "txt_filename";
            this.txt_filename.Size = new System.Drawing.Size(133, 22);
            this.txt_filename.TabIndex = 12;
            this.txt_filename.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 462);
            this.Controls.Add(this.txt_filename);
            this.Controls.Add(this.listBox_status);
            this.Controls.Add(this.cmd_filename);
            this.Controls.Add(this.cmd_file_delete);
            this.Controls.Add(this.cmd_file_copy);
            this.Controls.Add(this.cmd_file_write);
            this.Controls.Add(this.cmd_file_read_row);
            this.Controls.Add(this.cmd_file_read_byte);
            this.Controls.Add(this.cmd_zielpfad);
            this.Controls.Add(this.cmd_quellpfad);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.cmd_file_read_all);
            this.Controls.Add(this.cmd_end);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Dateihandˆing";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Button cmd_file_read_all;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.Button cmd_quellpfad;
        private System.Windows.Forms.Button cmd_zielpfad;
        private System.Windows.Forms.Button cmd_file_read_byte;
        private System.Windows.Forms.Button cmd_file_read_row;
        private System.Windows.Forms.Button cmd_file_write;
        private System.Windows.Forms.Button cmd_file_copy;
        private System.Windows.Forms.Button cmd_file_delete;
        private System.Windows.Forms.Button cmd_filename;
        private System.Windows.Forms.ListBox listBox_status;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TextBox txt_filename;
    }
}