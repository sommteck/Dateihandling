using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Dateihandling
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string quellverzeichnis, zielverzeichnis = @"C:\Temp.Test.txt", dateiname;

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_quellpfad_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            folderBrowserDialog1.SelectedPath = @"C:\Temp.Test.txt";
            //quellverzeichnis = folderBrowserDialog1.SelectedPath;
        }

        private void cmd_zielpfad_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            zielverzeichnis = folderBrowserDialog1.SelectedPath;
        }

        private void cmd_file_read_byte_Click(object sender, EventArgs e)
        {
            int zeichen;
            FileStream fs = new FileStream(@"C:\Temp\Text_2.txt", FileMode.Open);
            do
            {
                zeichen = fs.ReadByte();
                if (zeichen != -1)
                    txt_output.Text += (char)zeichen;
            }
            while (zeichen != -1);
            fs.Close();
            listBox_status.Items.Add("Datei wurde byteweise gelesen");

        }

        private void cmd_file_read_row_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream(@"C:\Temp\Text_2.txt", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string zeile;

            while (sr.Peek() != -1)
            {
                zeile = sr.ReadLine();
                txt_output.Text += zeile + "\r\n";
            }
            fs.Close();
            listBox_status.Items.Add("Datei zeilenweise gelesen");
        }

        private void cmd_file_read_all_Click(object sender, EventArgs e)
        {
            string[] zeilen;
            zeilen = File.ReadAllLines(@"C:\Temp\Text_2.txt");
            foreach (string s in zeilen)
            {
                txt_output.AppendText(s + "\r\n");
                // oder
                // txt_output.Text += s + "\r\n";
            }
            listBox_status.Items.Add("Datei wurde vollst‰ndig gelesen");
        }

        private void cmd_file_write_Click(object sender, EventArgs e)
        {

        }

        private void cmd_file_copy_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            quellverzeichnis = folderBrowserDialog1.SelectedPath;
            folderBrowserDialog1.ShowDialog();
            zielverzeichnis = folderBrowserDialog1.SelectedPath;

            if (!File.Exists(zielverzeichnis + "\\Test.txt"))
                File.Copy(quellverzeichnis + "\\Test.txt", zielverzeichnis + "\\Test.txt");
            else
                MessageBox.Show("Datei ist bereits vorhanden!");
            listBox_status.Items.Add("Datei in Verzeichnis " + zielverzeichnis + " kopiert.");
        }

        private void cmd_file_delete_Click(object sender, EventArgs e)
        {
            File.Delete(quellverzeichnis + "\\Test.txt");
            listBox_status.Items.Add("Datei Test.txt in Verzeichnis " + quellverzeichnis + " gelˆscht.");
        }

        private void cmd_filename_Click(object sender, EventArgs e)
        {
            txt_filename.Visible = true;
            dateiname = txt_filename.Text + ".txt";
            saveFileDialog1.ShowDialog();
            saveFileDialog1.InitialDirectory = "C:\\Temp\\" + dateiname;
            string filepath = saveFileDialog1.InitialDirectory;
            FileStream fs = new FileStream(filepath, FileMode.Create, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);



        }
    }
}